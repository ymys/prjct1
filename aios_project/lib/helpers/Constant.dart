const appName = 'Aios Indonesia';

const String appbartitle = 'Prime Web';

//change this url to set your URL in app
const String webinitialUrl = 'https://www.aios.co.id';

//keep local content of pages of setting screen
const String aboutPageURL = 'https://www.aios.co.id/halaman/detail/tentang-kami-aios';
const String privacyPageURL = 'https://www.aios.co.id/halaman/detail/kebijakan-privasi';
const String termsPageURL = 'https://www.aios.co.id/halaman/detail/perjanjian-pengguna';

//Change App id of android and IOS app
const String androidAppId = "com.iyaffle.rangoli";
const String iOSAppId = "585027354";

//To turn on/off ads
const bool showInterstitialAds = false;

//To turn on/off display of floating action button
const bool showNavigationBar = true;

//Ids for banner Ad
const androidBannerAdId = 'ca-app-pub-1886523041717389/1096576517';
const iosBannerAdId = 'ca-app-pub-1886523041717389/1096576517';

//Ids for interstitial Ad
const androidInterstitialAdId = 'ca-app-pub-3940256099942544/1033173712';
const iosInterstitialAdId = 'ca-app-pub-3940256099942544/1033173712';

//icon to set when get firebase messages
const String messageIcon = '@mipmap/ic_launcher_squircle';

//one-signal app id
const oneSignalAndroidAppId = '8a277392-cbf1-4f64-bd69-a9530730fe01';

//one-signal app id
const oneSignalIOSAppId = '8a277392-cbf1-4f64-bd69-a9530730fe01';

const String iconPath = 'assets/icons/';
