class CustomStrings {
  static const String home = 'Baranda';

  static const String demo = 'Link Terkait';

  static const String settings = 'Setting';

  static const String darkMode = 'Mode Gelap';

  static const String aboutUs = 'Tentang Kami';

  static const String privacyPolicy = 'Kebijakan Privasi';

  static const String terms = 'Aturan Pengguna';

  static const String share = 'Share';

  static const String rateUs = 'Beri Rating';

  //edit these 2 string for No Internet connectivity page
  static const String noInternet1 = 'No Internet Connection!';
  static const String noInternet2 = 'Mohon Coba Kembali';

  //edit these 2 strings for Page not found error page
  static const String pageNotFound1 = 'Ada yang salah!';
  static const String pageNotFound2 = 'Halaman tidak ditemukan';

  //edit these 2 strings for Incorrect URL error page
  static const String incorrectURL1 = 'Maaf kami tidak dapat menemukan halaman ini.';
  static const String incorrectURL2 = 'Mohon periksa URL Anda';

  //context messages for share app
  static const shareMessage =
      'Cek aplikasi jual beli online https://www.aios.co.id'; //Message body whille sharing content
  static const shareSubject =
      'Tempatnya jual beli gratis terpercaya!'; //subject to set for email conversation

// below 3 string are for Abouts Us, Privacy Policy and Terms & Conditions page pf Setting screen
  static const String aboutPageContent =
      '<html><body><h3>Tentang Kami</h3><p>Tentang Kami Aios Indonesia.</p></body></html>';
  static const String privacyPageContent =
      '<html><body><h3>Kebijakan Privasi</h3><p>Kebijakan Privasi pada Aios Indonesia Pasar Jual Beli Online.</p></body></html>';
  static const String termsPageContent =
      '<html><body><h3>Syarat Penggunaan</h3><p>Syarat penggunaan platform Jual Beli Aios Indonesia.</p></body></html>';
}
