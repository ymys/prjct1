import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import '../helpers/Strings.dart';
import '../widgets/no_internet.dart';
import '../widgets/no_internet_widget.dart';
import '../helpers/Constant.dart';
import '../widgets/load_web_view.dart';

class PrivacyPolicyScreen extends StatefulWidget {
  const PrivacyPolicyScreen({Key? key}) : super(key: key);
  static const routeName = '/privayPolicy';

  @override
  State<PrivacyPolicyScreen> createState() => _PrivacyPolicyScreenState();
}

class _PrivacyPolicyScreenState extends State<PrivacyPolicyScreen> {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;
  @override
  void initState() {
    super.initState();
    NoInternet.initConnectivity().then((value) => setState(() {
          _connectionStatus = value;
        }));
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      NoInternet.updateConnectionStatus(result).then((value) => setState(() {
            _connectionStatus = value;
          }));
    });
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String message = Theme.of(context).brightness == Brightness.dark
        ? "<font color='white'>" + CustomStrings.privacyPageContent + "</font>"
        : "<font color='black'>" + CustomStrings.privacyPageContent + "</font>";
    return Scaffold(
      appBar: AppBar(
        title: const Text(CustomStrings.privacyPolicy),
      ),
      body: privacyPageURL == ''
          ? Container(
              padding: const EdgeInsets.all(20),
              child: LoadWebView(message, false))
          : _connectionStatus == 'ConnectivityResult.none'
              ? const NoInternetWidget()
              : LoadWebView(privacyPageURL, true),
    );
  }
}
