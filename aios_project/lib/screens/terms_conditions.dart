import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import '../helpers/Strings.dart';
import '../widgets/no_internet.dart';
import '../widgets/no_internet_widget.dart';
import '../helpers/Constant.dart';
import '../widgets/load_web_view.dart';

class TermsConditions extends StatefulWidget {
  const TermsConditions({Key? key}) : super(key: key);
  static const routeName = '/terms';

  @override
  State<TermsConditions> createState() => _TermsConditionsState();
}

class _TermsConditionsState extends State<TermsConditions> {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;
  @override
  void initState() {
    super.initState();
    NoInternet.initConnectivity().then((value) => setState(() {
          _connectionStatus = value;
        }));
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      NoInternet.updateConnectionStatus(result).then((value) => setState(() {
            _connectionStatus = value;
          }));
    });
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String message = Theme.of(context).brightness == Brightness.dark
        ? "<font color='white'>" + CustomStrings.termsPageContent + "</font>"
        : "<font color='black'>" + CustomStrings.termsPageContent + "</font>";
    return Scaffold(
      appBar: AppBar(
        title: const Text(CustomStrings.terms),
      ),
      body: termsPageURL == ''
          ? Container(
              padding: const EdgeInsets.all(20),
              child: LoadWebView(message, false))
          : _connectionStatus == 'ConnectivityResult.none'
              ? const NoInternetWidget()
              : LoadWebView(termsPageURL, true),
    );
  }
}
